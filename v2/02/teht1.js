function mäenPisteytys(kPiste, pisteKerroin){
    return function(pituus){
        if(pituus>=kPiste){
            return 60+(pituus-kPiste)*pisteKerroin;
        }
        else{
            return 60-(kPiste-pituus)*pisteKerroin;
        }
    };
}

var normaaliLahti = mäenPisteytys(90, 2);
var suurmäkiLahti = mäenPisteytys(116, 1.8);

console.log("Mäkiennätyksen pisteet normaalimäessä: "+normaaliLahti(103.5));
console.log("Mäkiennätyksen pisteet suurmäessä: "+suurmäkiLahti(138));