const Immutable = require('immutable');

const Auto = (function(){
    
    const suojatut = new WeakMap();
    
    class Auto{
        constructor(){
            suojatut.set(this, {tankki: 0, matkamittari: 0});
        }
        
        getTankki(){
            return suojatut.get(this).tankki;
        }
        
        getMatkamittari(){
            return suojatut.get(this).matkamittari;
        }
        
        tankkaa(lisättäväMäärä){
            let polttoaineTankissa = suojatut.get(this).tankki;
            let matkamittariLukema = suojatut.get(this).matkamittari;
            suojatut.set(this, {tankki: polttoaineTankissa+lisättäväMäärä, matkamittari: matkamittariLukema});
        }
        
        aja(){
            if(suojatut.get(this).tankki>0){
                let polttoaineTankissa = suojatut.get(this).tankki;
                let matkamittariLukema = suojatut.get(this).matkamittari;
                suojatut.set(this, {tankki: polttoaineTankissa-1, matkamittari: matkamittariLukema+1})
            }
            else{
                console.log("Bensa on loppu!");
            }
        }
    }
    return Auto;
})();

const uusiauto = new Auto();
uusiauto.tankkaa(15);
console.log("Auton tankissa bensaa: "+uusiauto.getTankki());
uusiauto.aja();
uusiauto.aja();
uusiauto.aja();
uusiauto.aja();
console.log("Auton matkamittarin lukema: "+uusiauto.getMatkamittari()+" Autossa polttoainetta: "+uusiauto.getTankki());