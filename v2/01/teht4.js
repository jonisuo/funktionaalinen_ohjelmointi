var taulu2015 = [-10, -12, -3, -2, 5, 9, 20, 21, 15, 10, 7, -3];
var taulu2016 = [-8, -10, -5, -3, 4, 12, 19, 24, 15, 12, 5, 0];

var keskiarvot = [];

//Keskilämpötilat yhteen taulukkoon
for(var i=0; i<taulu2015.length; i++){
    keskiarvot.push((taulu2015[i]+taulu2016[i])/2);
}

console.log("Taulujen kuukausittaiset keskilämpötilat: "+keskiarvot);

//Filter
var filterCallback = function(alkio){
    return alkio>=0;
};

var filtteröity = keskiarvot.filter(filterCallback);

console.log("Filtteröidyt lämpötilat: "+filtteröity);

//Reduce
var reduceCallback = function(acc, curvalue, curindex, arr){
    if(curindex == arr.length-1){
        return (acc+curvalue)/arr.length;
    }
    else{
        return acc + curvalue;
    }
};

var keskilämpötila = filtteröity.reduce(reduceCallback);

console.log("Keskilämpötila: "+keskilämpötila);