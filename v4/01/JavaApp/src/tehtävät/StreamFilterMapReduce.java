//Tehtävä 2
//Alunperin filter kutsui lambdaa 10 kertaa ja map 5 kertaa
//Järjestys käännettynä filter ja map kutsuivat lambdaa 10 kertaa

package tehtävät;

import java.util.stream.IntStream;

public class StreamFilterMapReduce {
    private static int filterCounter=0, mapCounter=0;
    
    private static boolean filtteri(int x){
        filterCounter++;
        return x%2==0;
    }
    
    private static int mapperi(int x){
        mapCounter++;
        return x*3;
    }
    
    public static void main(String[] args) {
        //  sum of the triples of even integers from 2 to 10
        System.out.printf(
        "Sum of the triples of even integers from 2 to 10 is: %d%n",
        IntStream.rangeClosed(1,10)
            .map(x -> mapperi(x))
            .filter(x -> filtteri(x))
            .sum());
        
        System.out.println("Filter kutsui lambdaa: "+filterCounter+" kertaa");
        System.out.println("Map kutsui lambdaa: "+mapCounter+" kertaa");
    }
}