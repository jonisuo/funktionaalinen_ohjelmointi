function potenssiinKorotus(korotettava, potenssi){
    if(potenssi==0){
        return 1;
    }
    else{
        return korotettava * potenssiinKorotus(korotettava, potenssi-1);
    }
}

console.log("Tulos: "+potenssiinKorotus(2, 5));