function käännäLista(lista){
    let newList = [];
    let acc = lista.length-1;
    let sisäfunktio = function(lista, newList, acc){
        if(acc == 0){
            return newList;
        }
        else{
            newList.push(lista[acc])
            acc--;
            return sisäfunktio(lista, newList, acc);
        }
    }
    return sisäfunktio(lista, newList, acc);
}

var mylist = [0,1,2,3,4,5,6,7,8,9];

console.log("Lista käännettynä: "+käännäLista(mylist));
console.log("Alkuperäinen lista: "+mylist);