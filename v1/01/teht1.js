function onPalindromi(merkkijono){
    console.log("Kierroksen merkkijono "+merkkijono);
    if(merkkijono.length==0 || merkkijono.length==1){
        return true;
    }
    else if(merkkijono.charAt(0)!=merkkijono.charAt(merkkijono.length-1)){
        return false;
    }
    else{
        return onPalindromi(merkkijono.slice(1, merkkijono.length-1));
    }
}

console.log(onPalindromi("saippuakauppias"));