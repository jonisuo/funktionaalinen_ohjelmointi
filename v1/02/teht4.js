function potenssiinKorotus(korotettava, potenssi){
    return korotusHelper(korotettava, potenssi);
}

function korotusHelper(korotettava, potenssi){
    if(potenssi==0){
        return 1;
    }
    else{
        return korotettava * korotusHelper(korotettava, potenssi-1);
    }
}

console.log("Tulos: "+potenssiinKorotus(2, 5));