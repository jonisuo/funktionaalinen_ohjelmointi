const vertaa = function(){
    return function(a,b){
        if(a>b){
          return 1;  
        } 
        if(a<b){
          return -1;  
        } 
        else{
          return 0;  
        } 
    };
}();

const paramFunktio = function(vertaaFunktio, taulu1, taulu2){
    let acc = 0;
    let taulu1Isompi=0;
    let taulu2Isompi=0;
    let tulos;
    while(acc<taulu1.length){
        tulos = vertaaFunktio(taulu1[acc], taulu2[acc]);
        if(tulos == 1){
            taulu1Isompi++;
        }
        else if(tulos == -1){
            taulu2Isompi++;
        }
        acc++;
    }
    return "Taulu 1 keskilämpötila isompi: "+taulu1Isompi+" kertaa\nTaulu 2 keskilämpötila isompi: "+taulu2Isompi+" kertaa";
};

var taulu2015 = [-10, -12, -3, -2, 5, 9, 20, 21, 15, 10, 7, -3];
var taulu2016 = [-8, -10, -5, -3, 4, 12, 19, 24, 15, 12, 5, 0];

console.log(paramFunktio(vertaa, taulu2015, taulu2016));