const vertaa = function(){
    return function(a,b){
        if(a>b){
          return 1;  
        } 
        if(a<b){
          return -1;  
        } 
        else{
          return 0;  
        } 
    };
}();

console.log("Alkuarvot 1 ja 2. Vertaus tulos: " + vertaa(1,2));
console.log("Alkuarvot 2 ja 1. Vertaus tulos: " + vertaa(2,1));
console.log("Alkuarvot 2 ja 2. Vertaus tulos: " + vertaa(2,2));