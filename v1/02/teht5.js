'use strict';

var Moduuli = function() {
  let x=0;
  
  return {
    kasvata: function() {
      return x++;
    },
    vahenna: function() {
      return x--; 
    }
  }
}();

console.log('call to kasvata(): ' + Moduuli.kasvata()); 
console.log('call to kasvata(): ' + Moduuli.kasvata()); 
console.log('call to kasvata(): ' + Moduuli.kasvata()); 
console.log('call to kasvata(): ' + Moduuli.kasvata()); 
console.log('call to vahenna(): ' + Moduuli.vahenna());
console.log('call to vahenna(): ' + Moduuli.vahenna());
console.log('call to vahenna(): ' + Moduuli.vahenna());
console.log('call to vahenna(): ' + Moduuli.vahenna());
console.log('call to vahenna(): ' + Moduuli.vahenna());