;Tehtävä 1
(defn keskilampo
    []
    (def lampotilat2015 [-10 -12 -3 -2 5 9 20 21 15 10 7 -3])
    (def lampotilat2016 [-8 -10 -5 -3 4 12 19 24 15 12 5 0])
    (def keskilampotilat (map (fn [x y] (/ (+ x y) 2)) lampotilat2015 lampotilat2016))
    (println keskilampotilat)
)

;Tehtävä 2
(defn nesteytys
    []
    (def food-journal
        [{:kk 3 :paiva 1 :neste 5.3 :vesi 2.0}
        {:kk 3 :paiva 2 :neste 5.1 :vesi 3.0}
        {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
        {:kk 4 :paiva 5 :neste 5.0 :vesi 2.0}
        {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
        {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
        {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
        {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}])
    (def filtteröity (filter #(= (:kk %) 4) food-journal))
    (println filtteröity)
    
    (def mapatty (map #(- (:neste %) (:vesi %)) filtteröity))
    (println mapatty)
    
    (def nesteenkulutus (reduce + mapatty))
    (println nesteenkulutus)
)

;Tehtävä 3
(defn nesteytys2
    []
    (def food-journal
        [{:kk 3 :paiva 1 :neste 5.3 :vesi 2.0}
        {:kk 3 :paiva 2 :neste 5.1 :vesi 3.0}
        {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
        {:kk 4 :paiva 5 :neste 5.0 :vesi 2.0}
        {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
        {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
        {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
        {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}])
    (def filtteröity (filter #(= (:kk %) 4) food-journal))
    (println filtteröity)
    
    (def mapatty (into [] (map (fn [x] {:kk (get x :kk) :paiva (get x :paiva) :muuneste (- (get x :neste) (get x :vesi))}) filtteröity)))
    (println mapatty)
    
    (def nesteenkulutus (reduce + mapatty))
    (println nesteenkulutus)
)
