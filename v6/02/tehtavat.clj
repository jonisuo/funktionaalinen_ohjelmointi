;Tehtävä 1
(def sigFI (fn [nimi] (str "Erittäin formaali allekirjoitus - " nimi)))
(def sigEN (fn [nimi] (str "Very formal signature - " nimi)))

(defn formalFI 
    [nimi]
    (def allekirjoitus (partial sigFI))
    (allekirjoitus nimi)
)

(defn formalEN 
    [nimi]
    (def allekirjoitus (partial sigEN))
    (allekirjoitus nimi)
)


;Tehtävä 2
(def vektori [[1 2 3][4 5 6][7 8 9]])

(defn teht2a []
    (map (fn [x] (apply min x)) vektori)
)

(defn teht2b []
    (apply vector (map (fn [x] (apply min x)) vektori))
)


;Tehtävä 3
(def vampire-database
   {0 {:makes-blood-puns? false, :has-pulse? true  :name "McFishwich"}
    1 {:makes-blood-puns? false, :has-pulse? true  :name "McMackson"}
    2 {:makes-blood-puns? true,  :has-pulse? false :name "Damon Salvatore"}
    3 {:makes-blood-puns? true,  :has-pulse? true  :name "Mickey Mouse"}})

(defn lisaa-vampyyrikantaan 
    [db mbp hp nimi]
    (def avain (+ 1 (key (last db))))
    (def vampire-database (assoc db avain {:makes-blood-puns? mbp :has-pulse? hp :name nimi}))
    ;En keksinyt miten tässä "def vampire-database" korvattaisiin käyttämään db arvoa
)

(lisaa-vampyyrikantaan vampire-database true true "Uusi Vamppyyri1")
(lisaa-vampyyrikantaan vampire-database false true "Uusi Vamppyyri2")
(lisaa-vampyyrikantaan vampire-database true false "Uusi Vamppyyri3")
;(pprint vampire-database)


;Tehtövö 4
(defn poista-vampyyrikannasta
    [db poistettava]
    (def vampire-database (dissoc db poistettava))
)

(poista-vampyyrikannasta vampire-database 3)
(poista-vampyyrikannasta vampire-database 4)
(poista-vampyyrikannasta vampire-database 5)
(pprint vampire-database)
; =>{0 {:makes-blood-puns? false, :has-pulse? true, :name "McFishwich"},
;   1 {:makes-blood-puns? false, :has-pulse? true, :name "McMackson"},
;   2 {:makes-blood-puns? true, :has-pulse? false, :name "Damon Salvatore"},
;   6 {:makes-blood-puns? true, :has-pulse? false, :name "Uusi Vamppyyri3"}}