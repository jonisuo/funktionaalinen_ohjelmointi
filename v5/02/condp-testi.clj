;Tehtävä 1 ja 2
(defn parillinen?
    []
    (println "Anna positiivinen luku")
    (def x (Integer. (read-line)))
    (if (> x 0)
        (if (= (rem x 2) 0) 
            "truthy" 
            "falsey"
        )
        (do (println "Syötteen täytyy olla positiivinen")
            (recur)
        )
    )
)

;Tehtävä 3
(defn kolmellajaolliset
    [yläraja]
    (loop [x 3]
        (when(<= x yläraja)
            (println x)
            (recur(+ x 3))
        )
    )
)

;Tehtävä 4
(defn lotto
    []
    (loop [numerot (set[])]
        (if (= (count numerot) 7)
            (clojure.pprint/pprint numerot)
            (recur(conj numerot (+ 1 (rand-int 39)))) ;Vältetään numero 0 lisäämällä +1
        )
    )
)

;Tehtävä 5
(defn suurinyhttekijä
    [p q]
    (println p) ;Välitulosten tulostamista
    (if (= q 0)
        (println p)
        (recur q (rem p q))
    )
)
