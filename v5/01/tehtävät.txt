Tehtävä 2
    Alkuperäinen        Clojuressa
    (2*5)+4             (+ (* 2 5) 4)
    (1+2+3+4+5)         (+ 1 2 3 4 5)
    
    Funktion kutsu:
    ((fn [name] (str "Tervetuloa Tylypahkaan " name)) "Joni")
    
    Keskimmäisen nimen tulostus mapista:
    (get (get {:name {:first "Urho" :middle "Kaleva" :last "Kekkonen"}} :name) :middle)
    
    
Tehtävä 4
    Square funktion määrittely: (defn square [x] (* x x))
    

Tehtävä 5
    